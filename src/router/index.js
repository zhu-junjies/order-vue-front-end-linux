import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Center from "../components/Center";
import editPassword from "../components/editPassword";
import managerOrder from "../components/managerOrder";
import editShop from "../components/editShop";
import editShopMenu from "../components/editShopMenu";
import businessregister from "../components/businessregister";
import login from "../components/login";
import register from "../components/register";
import PersonalCenter from "../components/PersonalCenter";
import orderDishes from "../components/orderDishes";
import Rate from "../components/Rate";
import comsumerOrder from "../components/comsumerOrder";
import comunicate from "../components/comunicate";
import messagecotent from "../components/messagecotent";
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/register',
      name: 'register',
      component: register
    },
    {
      path: '/Rate',
      name: 'Rate',
      component: Rate
    },
    {
      path: '/orderDishes',
      name: 'orderDishes',
      component: orderDishes
    },
    {
      path: '/businessregister',
      name: 'businessregister',
      component: businessregister
    },
    {
      path: '/center',
      name: 'Center',
      component: Center,
      children:[{
        path: '/editPassword',
        name: 'editPassword',
        component: editPassword
      },
        {
          path: '/managerOrder',
          name: 'managerOrder',
          component: managerOrder
        }, {
          path: '/consumerOrder',
          name: 'consumerOrder',
          component: comsumerOrder
        },
        {
          path: '/PersonalCenter',
          name: 'PersonalCenter',
          component: PersonalCenter
        },
        {
          path: '/editShop',
          name: 'editShop',
          component: editShop
        },
        {
          path: '/comunicate',
          name: 'comunicate',
          component: comunicate,
          children:[{
            path:'/areas',
            name:'messagecotent',
            component: messagecotent,
          }]
        },
        {
          path: '/editShopMenu',
          name: 'editShopMenu',
          component: editShopMenu
        }
      ]
    },
  ]
})
