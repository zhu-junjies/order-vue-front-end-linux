// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
import axios from 'axios';
import qs from 'qs';
import 'bootstrap3/dist/css/bootstrap.min.css'
import 'element-ui/lib/theme-chalk/index.css';
import th from "element-ui/src/locale/lang/th";
Vue.config.productionTip = false;

Vue.use(ElementUI);
Vue.use(axios);
axios.defaults.withCredentials=true;
Vue.component('commonAside',{
  props:['isBus'],
  template:'        <div class="el_menus">\n' +
    '\n' +
    '        <el-menu\n' +
    '                default-active="2"\n' +
    '                class="el-menu-vertical-demo"\n' +
    '                background-color="#545c64"\n' +
    '                text-color="#fff"\n' +
    '                background-color="#545c64"\n' +
    'router="true"\n'+
    '               >\n' +
    '          <el-submenu index="1">\n' +
    '            <template slot="title">\n' +
    '              <i class="el-icon-location"></i>\n' +
    '              <span>个人中心</span>\n' +
    '            </template>\n' +
    '            <el-menu-item-group>\n' +
    '              <el-menu-item index="1-1" route="/PersonalCenter" >个人资料修改</el-menu-item>\n' +
    '              <el-menu-item index="1-2" route="/editPassword">修改密码</el-menu-item>\n' +
    '              <el-menu-item index="1-3" route="/consumerOrder">订单查看</el-menu-item>\n' +
    '            </el-menu-item-group>\n' +
    '          </el-submenu>\n' +
    '          <el-submenu index="2" v-if="this.isBus==true">\n' +
    '            <template slot="title">\n' +
    '              <i class="el-icon-location"></i>\n' +
    '              <span >商家中心</span>\n' +
    '            </template>\n' +
    '            <el-menu-item-group>\n' +
    '              <el-menu-item index="2-1" route="/managerOrder">订单配送管理</el-menu-item>\n' +
    '              <el-submenu index="2-4">\n' +
    '                <template slot="title">商店管理</template>\n' +
    '                <el-menu-item index="2-4-1" route="/editShop">商店资料修改</el-menu-item>\n' +
    '                <el-menu-item index="2-4-2" route="/editShopMenu">商店菜单管理</el-menu-item>\n' +
    '              </el-submenu>\n' +
    '            </el-menu-item-group>\n' +
    '          </el-submenu>\n' +
    '          <el-menu-item index="3" route="/comunicate">\n' +
    '            <template slot="title">\n' +
    '              <i class="el-icon-chat-line-round"></i>\n' +
    '              <span>查看消息</span>\n' +
    '            </template>\n' +
    '              </el-menu-item>\n' +
    '        </el-menu>\n' +
    '        </div>'
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  data:{
    loginUsername:'',
    loginStatus:false,
    isBus:localStorage.getItem("isBus")
  },
  router:router,
  components: { App },
  template: '<App/>'
})
